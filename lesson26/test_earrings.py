import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class TestEarrings():
  def setup_method(self, method):
    self.driver = webdriver.Chrome("chromedriver.exe")
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()

  @pytest.mark.parametrize("text",(("сережки"),("окуляри"),("стіна")))
  def test_search_earrings(self, text):
    self.driver.get("https://a-shop.ua/")
    self.driver.set_window_size(1500, 824)
    self.driver.find_element(By.ID, "search").click()
    self.driver.find_element(By.ID, "search").send_keys(text)
    self.driver.find_element(By.ID, "search").send_keys(Keys.ENTER)

    WebDriverWait(self.driver, 10).until(
      EC.visibility_of_element_located((By.CSS_SELECTOR, "#maincontent")))
    elements = self.driver.find_elements(By.PARTIAL_LINK_TEXT, (text.capitalize()))
    assert len(elements) > 2, f"Amount of elements less than 2"




  
