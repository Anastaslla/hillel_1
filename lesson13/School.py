import datetime
from abc import ABC, abstractmethod


class Person(ABC):

    def __init__(self,
                 name: str,
                 surname: str,
                 birth_date: tuple,
                 ):
        self.name = name
        self.surname = surname
        self.birth_date = datetime.date(*birth_date)


class Stuff(Person):
    def __init__(self, name: str, surname: str, birth_date: tuple):
        super().__init__(name, surname, birth_date)

    @abstractmethod
    def paying_salary(self):
        pass


class Director(Stuff):
    def __init__(self, name: str, surname: str, birth_date: tuple):
        super().__init__(name, surname, birth_date)

    def paying_salary(self):
        return 20000, "UAH"


class HeadTeacher(Stuff):

    def __init__(self, name: str, surname: str, birth_date: tuple):
        super().__init__(name, surname, birth_date)

    def paying_salary(self):
        return 15000, "UAH"


class Teacher(Stuff):
    teacher_list = []

    def __init__(self, name: str, surname: str, birth_date: tuple):
        super().__init__(name, surname, birth_date)
        self.__class__.teacher_list.append(self)

    def paying_salary(self):
        return 6000, "UAH"


class Student(Stuff):
    def __init__(self, name: str, surname: str, birth_date: tuple, salary: int):
        super().__init__(name, surname, birth_date)

    def paying_salary(self):
        return 90, "Points"


class School:
    def __init__(self):
        pass
    def count_class_points(self, school_class):
        if school_class.students_list != []:
            student_points = [i.paying_salary()[0] for i in school_class.students_list]
            average_points = sum(student_points)/len(student_points)
            return average_points

    def count_school_points(self, list_of_classes):
        not_empty_classes = 0
        sum_of_points = 0
        for i in list_of_classes:
            if i.students_list != []:
                not_empty_classes += 1
                sum_of_points += self.count_class_points(i)
        average_school_points = sum_of_points/not_empty_classes
        return average_school_points


class SchoolClass(School):
    list_of_classes = []
    limit = 10

    def __init__(self, name, teacher):
        super().__init__()
        self.__class__.list_of_classes.append(self)
        self.name = name
        self.class_teacher = teacher
        self.students_list = []

    def add_student(self, new_student):
        if len(self.students_list) < self.__class__.limit:
            self.students_list.append(new_student)
            return "Student is added"
        else:
            return "Current class is full, try add to another class"

    def move_student(self, student, to_class):
        self.students_list.remove(student)
        to_class.add_student(student)

    def remove_student(self, student):
        self.students_list.remove(student)
        return "Student was removed"


def count_school_budget(class_limit, teachers, *args):
    all_stuff_salary = 0
    for i in args:
        all_stuff_salary += i.paying_salary()[0]
    students_amount = len(teachers.teacher_list)*class_limit
    price_per_student = round(all_stuff_salary / students_amount)
    return f"{students_amount} students, {price_per_student} UAH per each"
