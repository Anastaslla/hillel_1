import School as S

chief = S.Director("Anne", "Nikolas", (1989, 1, 20))
head_teacher = S.HeadTeacher("Stass", "Nikolas", (1989, 1, 20))
teacher1 = S.Teacher("Antonio", "Nikolas", (1989, 1, 20))
teacher2 = S.Teacher("Alex", "Nikolas", (1989, 1, 20))
teacher3 = S.Teacher("Xelo", "Salo", (1989, 1, 20))
student1 = S.Student("Ai", "Nikolas", (1989, 1, 20), 100)
student2 = S.Student("Bi", "Nikolas", (1989, 1, 20), 100)
student3 = S.Student("Ci", "Nikolas", (1989, 1, 20), 100)
student4 = S.Student("Di", "Nikolas", (1989, 1, 20), 100)

price_per_student = S.count_school_budget(S.SchoolClass.limit, S.Teacher, chief, head_teacher, teacher1, teacher2)
print(price_per_student)

A = S.SchoolClass("A", teacher1)
B = S.SchoolClass("A", teacher2)
A.add_student(student1)
A.add_student(student2)
A.add_student(student3)
B.add_student(student4)


print("school_points", A.count_school_points(A.list_of_classes))
print("class_A_points", A.count_class_points(A))