amount = int(input())
dict = {}
for i in range(amount):
    input_data = input().split()
    if dict.get(input_data[0]) == None:
        dict[input_data[0]] = input_data[1]
    else:
        dict[input_data[0]] = int(dict.get(input_data[0])) + int(input_data[1])
for i in sorted(dict):
    print(i, dict[i])
