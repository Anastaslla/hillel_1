rows = int(input())
words = []
dict = {}
for i in range(rows): # Собираем все строки в один список слов
    words.extend(input().split())
for i in set(words): # Считаем количество уникальных слов и сохраняем ключ - количество, значение - список слов
    if words.count(i) in dict:
       dict[words.count(i)].append(i)
    else: 
        dict[words.count(i)] = [i]
max = max(dict.keys())
print(sorted(sorted(dict[max]), key=str.upper)[0])