list = [i for i in input().split()]
uniq = set()
for i in list:
    if i in uniq:
        print("YES")
    else:
        print("NO")
        uniq.add(i)