from dataclasses import dataclass
from abc import abstractmethod


def add_supplement(cls):
	def wrapper():
		def add_supplement(self, *args):
			additional_price = sum([arg.price for arg in args])
			old_price = self.count_price()
			new_price = {key: value + additional_price for key, value in old_price.items()}

			return new_price

		cls.add_supplement = add_supplement

		return cls

	return wrapper()


@dataclass
class Beverage:
	name: str
	description: str
	price: float

	@abstractmethod
	def count_price(self):
		pass

@dataclass
@add_supplement
class Tea:
	name: str
	description: str
	gen_price: float

	def count_price(self):
		return {"small": self.gen_price / 100 * 70, "medium": self.gen_price, "big": self.gen_price / 100 * 115}



@dataclass
@add_supplement
class Coffee:
	name: str
	description: str
	gen_price: float

	def count_price(self):
		return {"small": self.gen_price / 100 * 85, "medium": self.gen_price, "big": self.gen_price / 100 * 125}


@dataclass
class Additions:
	name: str
	price: float


marshmallow = Additions("Marshmallow", 15.0)
milk = Additions("Milk", 5.0)
cream = Additions("Cream", 10.0)

tea = Tea("GreenTea", "Green huge leaves tea", 35.0)
coffee = Coffee("CoffeeN", "Best dark coffee", 50.0)

print("Coffee",coffee.count_price())
print("Coffe+Milk+Cream",coffee.add_supplement(milk, cream))
print("Coffe+Milk+Cream+Marshamllow",coffee.add_supplement(milk, cream, marshmallow))