
class check_multiplier:
	def __init__(self, multiplier):
		self.multiplier = multiplier

	def __call__(self, func):
		def wrapper(*args):
			new_arg = [arg % self.multiplier == 0 for arg in func(*args)]
			return new_arg
		return wrapper

@check_multiplier(3)
def fun(a, b, c):
	return a*2, b*3, c*4

print(fun(2, 2, 2))
