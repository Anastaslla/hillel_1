class add_text:
	def __init__(self, txt_before, txt_after):
		self.txt_before = txt_before
		self.txt_after = txt_after

	def __call__(self, func):
		def wrapper(*args):
			new_text= func(*args).replace(self.txt_before, self.txt_after)
			return new_text
		return wrapper

@add_text("Hi", "Hello")
def fun(a):
	return f"Hi {a}. Hi everyone!"

print(fun("Anne"))