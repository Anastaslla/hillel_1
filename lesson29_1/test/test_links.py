import time

import pytest
from selenium.webdriver import Chrome, Keys


class TestBrowser:
    links = link1="https://github.com/"
    link1_title="GitHub: Where the world builds software · GitHub"
    link2="https://about.gitlab.com/"
    link2_title="The One DevOps Platform | GitLab"

    @pytest.mark.browser
    def test_back_forward(self, browser):
        browser.get(self.link1)
        assert browser.current_url == self.link1, f"Incorrect URL for the link1: {browser.current_url}. Should be: {self.link1}"
        assert browser.title == self.link1_title, f"Incorrect title: {browser.title}, should be: {self.link1_title}"

        browser.get(self.link2)
        assert browser.current_url == self.link2, f"Incorrect URL for the link2: {browser.current_url}. Should be: {self.link2}"
        assert browser.title == self.link2_title, f"Incorrect title: {browser.title}, should be: {self.link2_title}"

        browser.back()
        assert browser.current_url == self.link1, f"Incorrect URL for the link1: {browser.current_url}. Should be: {self.link1}"
        assert browser.title == self.link1_title, f"Incorrect title: {browser.title}, should be: {self.link1_title}"

        browser.forward()
        assert browser.current_url == self.link2, f"Incorrect URL for the link2: {browser.current_url}. Should be: {self.link2}"
        assert browser.title == self.link2_title, f"Incorrect title: {browser.title}, should be: {self.link2_title}"

    @pytest.mark.browser
    def test_switch_tab(self, browser):
        browser.get(self.link1)
        assert browser.current_url == self.link1, f"Incorrect URL for the link1: {browser.current_url}. Should be: {self.link1}"
        assert browser.title == self.link1_title, f"Incorrect title: {browser.title}, should be: {self.link1_title}"

        browser.switch_to.new_window('tab')
        browser.get(self.link2)
        assert browser.current_url == self.link2, f"Incorrect URL for the link2: {browser.current_url}. Should be: {self.link2}"
        assert browser.title == self.link2_title, f"Incorrect title: {browser.title}, should be: {self.link2_title}"

        browser.switch_to.window(browser.window_handles[0])
        assert browser.current_url == self.link1, f"Incorrect URL for the link1: {browser.current_url}. Should be: {self.link1}"
        assert browser.title == self.link1_title, f"Incorrect title: {browser.title}, should be: {self.link1_title}"

