import mysql.connector
import lesson28.passw as p


def create_database(username, password, db_name, host='127.0.0.1'):
    connector = mysql.connector.connect(user=username, password=password, host=host)
    cursor = connector.cursor()
    try:
        cursor.execute(f"DROP database IF EXISTS {db_name}")
        create_query = f"CREATE DATABASE {db_name}"
        cursor.execute(create_query)
    finally:
        connector.close()


if __name__ == "__main__":
    create_database(**p.credits, db_name="SCHOOL")
