import mysql.connector
import lesson28.passw as p

from lesson28.example.helper import read_csv, form_insert_from_dict_tuple


def insert_data(username, password, db_name, host='127.0.0.1', port=3306):
    connector = mysql.connector.connect(user=username, password=password, host=host, port=3306, database=db_name)
    cursor = connector.cursor()
    people = read_csv("people.csv")
    stuff = read_csv("stuff.csv")
    school_classes = read_csv("school_classes.csv")

    #Insert people
    cursor.execute(form_insert_from_dict_tuple("people", people))
    connector.commit()

    # Insert stuff
    cursor.execute(form_insert_from_dict_tuple("stuff", stuff))
    connector.commit()

    # Insert school class
    cursor.execute(form_insert_from_dict_tuple("school_classes", school_classes))
    connector.commit()

if __name__ == "__main__":
    insert_data(**p.credits, db_name="SCHOOL")