import mysql.connector
import click
from mysql.connector import ProgrammingError
import lesson28.passw as p

def search_stuff(username, password, position, host='127.0.0.1', port=3306):
    connector = mysql.connector.connect(user=username, password=password, host=host, port=port)
    cursor = connector.cursor()
    try:
        cursor.execute("""
        SELECT position, base_salary, person_name, person_surname, class_name 
        FROM school.stuff AS stuff LEFT JOIN school.people AS people
        ON stuff.person_id = people.person_id LEFT JOIN school.school_classes AS class
        ON stuff.person_id = class.person_id GROUP BY stuff.id
        """)
    except ProgrammingError:
        print(f"Bad query: {cursor.statement}")

    result = cursor.fetchall()

    if not result:
        print("No shop is found or no items in it")
    else:
        print(f"List of stuff: {position}")
        for it in result:
            print(*it)

    connector.close()


if __name__ == "__main__":
    search_stuff(**p.credits, position="Teacher")