import mysql.connector
import click
from mysql.connector import ProgrammingError
import lesson28.passw as p


def search_students_in_class(username, password, class_name, host='127.0.0.1', port=3306):
    connector = mysql.connector.connect(user=username, password=password, host=host, port=port)
    cursor = connector.cursor()
    try:
        cursor.execute("""
        SELECT person_name, person_surname, birth_date, sex FROM school.people as people LEFT JOIN school.school_classes as class
        ON people.person_id = class.person_id
        WHERE class.class_name =  %(class_name)s AND class.role = "Student";
        """, {"class_name": class_name})
    except ProgrammingError:
        print(f"Bad query: {cursor.statement}")

    result = cursor.fetchall()

    if not result:
        print("No shop is found or no items in it")
    else:
        print(f"Student list in class: {class_name}")
        for it in result:
            print(*it)

    connector.close()

if __name__ == "__main__":
    search_students_in_class(**p.credits, class_name="8a")