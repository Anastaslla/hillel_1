import mysql.connector
import passw as p


def create_school_tables(username, password, db_name, host='127.0.0.1'):
    connector = mysql.connector.connect(user=username, password=password, host=host, database=db_name)
    cursor = connector.cursor()


    cursor.execute(f"""
                CREATE TABLE people (
            person_id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            person_name VARCHAR(50) NOT NULL,
            person_surname VARCHAR(50) NOT NULL,
            birth_date DATE NOT NULL,
            sex VARCHAR(50) NOT NULL
            );
                """)

    cursor.execute(f"""
                CREATE TABLE stuff (
            id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            position VARCHAR(100) NOT NULL,
            base_salary INT NOT NULL,
            person_id INT NOT NULL,
            FOREIGN KEY (person_id) REFERENCES people(person_id)
            );
                """)

    cursor.execute(f"""
                CREATE TABLE school_classes (
            class_name VARCHAR(50) NOT NULL,
            role VARCHAR(50) NOT NULL,
            person_id INT NOT NULL,
            FOREIGN KEY (person_id) REFERENCES people(person_id)
            );
                """)


if __name__ == "__main__":
    create_school_tables(**p.credits, db_name="SCHOOL")