from dataclasses import dataclass

a,b,c = 1,2,3
a,b = b,a
print(a,b,c)

names = ["Anne", "Alex", "Patrick"]
surnames = ["Nikolas", "Lee", "Joy"]
dict = dict(zip(names, surnames))
print(dict)

@dataclass
class User:
    username: str
    age: str

user_data = {"username":"Alex", "age":16}

user = User(**user_data) # username = "Alex", age=16 это удобно, когда мы получили данные в json
print(user.username)

import json
with open("file.txt","w") as f:
    json.dump(user_data, f)

json.dumps(dict) # получаем json строку из dict
json.dump(dict, f)  # Записываем в файл (из файла)
json.loads() # десериализируем строку (в дикт)
json.load() # десериализируем из файла


