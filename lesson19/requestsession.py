import requests
from pprint import pprint

headers = {
    'accept': 'application/json',
    'Content-Type': 'application/json'
}

with requests.Session() as http_session:
  http_session.headers.update(headers)
  pet_info1 = {
  "id": 12,
  "name": "Another Doggie",
  "category": {
    "id": 1,
    "name": "Dogs"
  },
  "photoUrls": [
    "string"
  ],
  "tags": [
    {
      "id": 0,
      "name": "string"
    }
  ],
  "status": "available"
  }

  http_session.post("https://petstore3.swagger.io/api/v3/pet", json=pet_info1)

  pprint(http_session.get('https://petstore3.swagger.io/api/v3/pet/12').json())

  pet_to_update1 = {
  "id": 12,
  "name": "Yet Another Doggie 1",
  "category": {
    "id": 1,
    "name": "Dogs"
  },
  "photoUrls": [
    "string"
  ],
  "tags": [
    {
      "id": 0,
      "name": "my_doggy"
    }
  ],
  "status": "available"
  }

  http_session.put('https://petstore3.swagger.io/api/v3/pet', json=pet_to_update1)

  print("===================")
  pprint(http_session.get('https://petstore3.swagger.io/api/v3/pet/12').json())