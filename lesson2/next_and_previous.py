n = int(input())
next_num = n+1
previous_num = n-1

print("The next number for the number", n, "is", str(next_num) + ".")
print("The previous number for the number", n, "is", str(previous_num) + ".")
