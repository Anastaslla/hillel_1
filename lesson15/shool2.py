from datetime import date
from dataclasses import dataclass, field
from abc import ABC, abstractmethod
from enum import Enum


class Sex(Enum):
    MALE = (0, "Male")
    FEMALE = (1, "Female")

    def __init__(self, id, name):
        self.number = id
        self._name = name

@dataclass
class Person(ABC):
    name: str
    surname: str
    sex: Sex
    birth_date: date


    @property
    def age(self):
        return (date.today() - self.birth_date).days // 365


@dataclass
class Stuff(Person):
    salary: float

    @abstractmethod
    def paying_salary(self):
        pass


@dataclass
class Chief(Stuff):
    def paying_salary(self):
        return self.salary # тут сложная система расчета зп с учетом бонусов


@dataclass
class HeadTeacher(Stuff):
    def paying_salary(self):
        return self.salary


@dataclass
class Teacher(Stuff):
    def paying_salary(self):
        return self.salary


@dataclass
class Student(Stuff):
    points: int

    def __post_init__(self):
        self.salary = 0

    def paying_salary(self):
        return self.salary


@dataclass
class CleaningStuff(Stuff):
    def paying_salary(self):
        return self.salary


@dataclass
class SchoolClass():
    class_name: str
    class_teacher: Teacher
    student_list: list = field(default_factory = list)
    list_of_classes = []

    def __post_init__(self):
        self.list_of_classes.append(self)

    def add_student(self, *args: Student):
        for student in args:
            self.student_list.append(student)
        return [i.name for i in self.student_list]

    def move_student(self, student, to_class):
        self.student_list.remove(student)
        to_class.add_student(student)
        return f"Student was removed to {to_class}"

    def remove_student(self, student):
        self.student_list.remove(student)
        return "Student was removed"

    def delete_class(self, school_class):
        self.list_of_classes.remove(school_class)
        return f"{school_class} was removed"


@dataclass
class PayingSystem:
    def mounthly_pay(self, stuff_list, student_list):
        if student_list == []:
            raise EmptyGroupException()
        all_stuff_salary = 0
        for stuff in stuff_list:
            all_stuff_salary += stuff.paying_salary()
        price_per_student = round(all_stuff_salary / len(student_list))
        return f"Monthly fee is  {price_per_student} UAH per student"


class EmptyGroupException(Exception):
    pass

@dataclass
class EvaluationSystem:
    list_of_classes: list

    def count_class_points(self, school_class):
        if school_class.student_list == []:
            raise EmptyGroupException(f"School class '{school_class.class_name}' has no students.")
        else:
            student_points = [student.points for student in school_class.student_list]
            average_points = sum(student_points) / len(student_points)
        return {school_class.class_name: average_points}

    def count_school_points(self):
        average_points_per_class = {}
        empty_classes = []
        for school_class in self.list_of_classes:
            try:
                average_points_per_class.update(self.count_class_points(school_class))
            except EmptyGroupException:
                empty_classes.append(school_class.class_name)
        average_school_points = sum([val for val in average_points_per_class.values()]) / len(average_points_per_class)
        return f"School average points: {average_school_points}\nEach class average points {average_points_per_class}, " \
               f"Empty classes: {','.join(empty_classes)}"


chief = Chief("AAA", "Aaaa", Sex.MALE, date(1965, 1, 1), 20000)
head_teacher = HeadTeacher("BBB", "Bbbb", Sex.MALE, date(1989, 1, 20), 15000)
teacher1 = Teacher("Teacher1name", "Teacher1surn", Sex.FEMALE, date(1976, 1, 20), 6000)
teacher2 = Teacher("Teacher2name", "Teacher2surn", Sex.FEMALE, date(1976, 1, 20), 6000)
teacher3 = Teacher("Teacher3name", "Teacher3surn", Sex.MALE, date(1975, 1, 20), 6000)

s1 = Student("Student1name", "Student1surn", Sex.MALE, date(1996, 6, 15), 0, 90)
s2 = Student("Student2name", "Student2surn", Sex.MALE, date(1996, 8, 12), 0, 90)
s3 = Student("Student3name", "Student3surn", Sex.FEMALE, date(1996, 6, 11), 0, 90)
anne_n = Student("Anne", "Nikolas", Sex.FEMALE, date(1989, 4, 20), 0, 99)
stas_n = Student("Stas", "Nikolas", Sex.MALE, date(1983, 12, 25), 0, 99)
antonio_c = Student("Antonio", "Country", Sex.MALE, date(1982, 2, 25), 0, 70)

A = SchoolClass("A", teacher1)
B = SchoolClass("B", teacher2)
C = SchoolClass("C", teacher3)

A.add_student(anne_n, antonio_c, stas_n, s1, s2, s3)
A.move_student(antonio_c, B)
A.remove_student(stas_n)


print(A.class_teacher.name, [i.name for i in A.student_list])
print(B.class_teacher.name, [i.name for i in B.student_list])

paying_system2022 = PayingSystem()
eval2022_02 = EvaluationSystem([A,B,C])

print(paying_system2022.mounthly_pay([chief, head_teacher, teacher1, teacher2, teacher3], [s1, s2, s3, anne_n, antonio_c, stas_n]))
print(eval2022_02.count_school_points())
# print(eval2022_02.count_class_points(C))
# print(stas_n.sex.name)