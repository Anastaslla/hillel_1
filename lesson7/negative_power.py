a = float(input())
n = int(input())


def power_plus(a, n):
    if n == 1:
        return a
    else:
        return a * power_plus(a, n - 1)


def negative_power(a, n):
    if n == 0:
        result = 1
    elif n > 0:
        result = power_plus(a, n)
    else:
        result = 1 / power_plus(a, -n)
    return '{0:g}'.format(result)


print(negative_power(a, n))