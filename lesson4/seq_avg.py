digits = 0
i = 0
while True:
    n = int(input())
    if n == 0:
        break
    else:
        digits += n
        i += 1

if i > 0:
    avg = digits / i
    print(avg)
