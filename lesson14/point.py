class Point:
    def __init__(self, x=0, y=0, z=0):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return f"({self.x},{self.y},{self.z})"

    def __add__(self, other):
        if isinstance(other, Point):
            x = self.x + other.x
            y = self.y + other.y
            z = self.z + other.z
            return Point(x, y, z)
        elif isinstance(other, int):
            x = self.x + other
            y = self.y + other
            z = self.z + other
        else:
            raise TypeError(f"Unsapported type {type(other)}")
        return Point(x, y, z)

    def __mul__(self, other):
        if isinstance(other, Point):
            x = self.x * other.x
            y = self.y * other.y
            z = self.z * other.z
            return Point(x, y, z)
        elif isinstance(other, int):
            x = self.x * other
            y = self.y * other
            z = self.z * other
        else:
            raise TypeError(f"Unsapported type {type(other)}")
        return Point(x, y, z)

    def __sub__(self, other):
        if isinstance(other, Point):
            x = self.x - other.x
            y = self.y - other.y
            z = self.z - other.z
            return Point(x, y, z)
        elif isinstance(other, int):
            x = self.x - other
            y = self.y - other
            z = self.z - other
        else:
            raise TypeError(f"Unsapported type {type(other)}")
        return Point(x, y, z)

    def __truediv__(self, other):
        if isinstance(other, Point):
            x = self.x//other.x
            y = self.y // other.y
            z = self.z // other.z
            return Point(x, y, z)
        elif isinstance(other, int):
            x = self.x // other
            y = self.y // other
            z = self.z // other
        else:
            raise TypeError(f"Unsapported type {type(other)}")
        return Point(x, y, z)


p1 = Point(1, 2, 3)
p2 = Point(1, 1, 1)
print(p1+p2)
print(p1 + 4)
print(p1 * p2)
print(p1 * 2)
print(p1 - p2)
print(p1 - 2)
print(p1 / p2)
print(p1 / 2)


