from abc import ABC, abstractmethod


class Transport(ABC):
    def __init__(self, name: str, speed: float, control: str, moving_element: str):
        self.name = name
        self.speed = speed # km/s
        self.control = control
        self.moving_element = moving_element

    @abstractmethod
    def start(self):
        pass

    @abstractmethod
    def turn(self, side):
        pass

    @abstractmethod
    def stop(self):
        pass

    @abstractmethod
    def count_time(self, distance):
        pass
    
    @abstractmethod
    def move(self, surface, distance):
        pass


class Car(Transport):
    def __init__(self, name: str, speed: float, control="steering wheel", moving_element="wheels"):
        super().__init__(name, speed, control, moving_element)

    def start(self):
        return f"{self.name} starts driving."

    def turn(self, side):
        return f"It turns {side}"

    def stop(self):
        return f"It stops driving."

    def drive(self):
        return f"It's driving."

    def count_time(self, distance):
        return round(distance*3600/self.speed, 2)
    
    def move(self, surface, distance):
        if surface.lower() == "ground":
            text = f"{self.start()} {self.drive()} {self.stop()}... It takes {self.count_time(distance)}h."
        else:
            text = f"{self.name} can't move on {surface}"
        return text


class Aircraft(Transport):
    def __init__(self, name: str, speed: float, control="yoke", moving_element="ailerons"):
        super().__init__(name, speed, control, moving_element)
        self.start_time = 0.5
        self.stop_time = 0.5

    def start(self):
        return f"{self.name} takes off."

    def stop(self):
        return f"It lands....stops moving."

    def turn(self, side):
        return f"It turns {side}"

    def fly(self):
        return f"It's flying."

    def count_time(self, distance):
        time = round((self.start_time + distance*3600/self.speed + self.stop_time), 2)
        return time

    def move(self, surface, distance):
        if surface.lower() == "ground":
            print(distance)
            print(self.speed)
            text = f"{self.start()} {self.fly()} {self.stop()}... It takes {self.count_time(distance)}h."
        else:
            text = f"{type(self)} can't move on {surface}"
        return text


class Ship(Transport):
    def __init__(self, name: str, speed: float, control="helm", moving_element="propeller"):
        super().__init__(name, speed, control, moving_element)

    def start(self):
        return f"{self.name} starts swimming."

    def stop(self):
        return f"It stops swimming."

    def turn(self, side):
        return f"It turns {side}"

    def swim(self):
        return f"It's swimming."

    def count_time(self, distance):
        time = round(distance*3600/self.speed, 2)
        return time

    def move(self, surface, distance):
        if surface.lower() == "water":
            text = f"{self.start()} {self.swim()} {self.stop()}... It takes {self.count_time(distance)}h."
        else:
            text = f"{self.name} can't move on {surface}"
        return text


class BMP(Car, Ship):
    def __init__(self, name: str, speed: float, water_speed: float):
        Car.__init__(self, name, speed)
        self.water_speed = water_speed

    def count_time_water(self, distance):
        time = round(distance*3600/self.water_speed, 2)
        return time

    def move(self, surface, distance):
        if surface.lower() == "ground":
            text = f"{Car.start(self)} {Car.drive(self)} {Car.stop(self)}... It takes {Car.count_time(self, distance)}h."
        elif surface.lower() == "water":
            text = f"{Ship.start(self)} {Ship.swim(self)} {Ship.stop(self)}... It takes {self.count_time_water(distance)}h."
        else:
            text = f"{self.name} can't move on {surface}"
        return text


class Automotive(Aircraft, Car):
    def __init__(self, name: str, speed: float, driving_speed: float):
        Aircraft.__init__(self, name, speed)
        self.driving_speed = driving_speed

    def count_time_driving(self, distance):
        time = round(distance*3600/self.driving_speed, 2)
        return time

    def move(self, surface, distance):
        if surface.lower() == "ground":
            if distance < 100:
                text = f"{Car.start(self)} {Car.drive(self)} {Car.stop(self)}... It takes {self.count_time_driving(distance)}h."
            else:
                text = f"{Aircraft.start(self)} {Aircraft.fly(self)} {Aircraft.stop(self)}... It takes {Aircraft.count_time(self, distance)}h."
        else:
            text = f"{self.name} can't move on {surface}"
        return text


flight = Aircraft("AN-124", 800.0*3600)
car = Car("BMW03", 337.0*3600)
ship = Ship("Skjold class", 83.0*3600)
bmp = BMP("BMP03", 70.0*3600, 10.0*3600)
automotive = Automotive("OM-KL2", 170.0*3600, 270.0*3600)

print(flight.move("ground", 200))
print(bmp.move("water", 200))
print(bmp.move("ground", 200))
print(automotive.move("water", 200))
print(automotive.move("ground", 200))

print(automotive.turn("left"))