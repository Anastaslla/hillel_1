title = "CEO Red Bull Inc.\nMr. John Bigbull\n"
first_name = "Anne"
surname = "Nikolas"
from_date = "2022.01.11"
to_date = "2022.01.13"


def add_title(func):
    def wrapper(*args, **kwargs):
        return title + "\n" + func(*args, **kwargs)

    return wrapper


@add_title
def vacation_pattern(name, surname, from_date, to_date):
    content_text = f"Hi John,\nI need the paid vacation from {from_date} to {to_date}.\n{name} {surname}"

    return content_text

@add_title
def sick_leave_pattern(name, surname, from_date, to_date):
    content_text = f"Hi John,\nI need the paid sick leave from {from_date} to {to_date}.\n{name} {surname}"

    return content_text


@add_title
def day_off_pattern(name, surname, from_date, to_date):
    content_text = f"Hi John,\nI need the paid day off from {from_date} to {to_date}.\n{name} {surname}"

    return content_text


def select_vacation():
    vacation_type = input("Please, input number\n1:Vacation, 2:Sick leave or 3:Day off: ")
    if vacation_type == "1" or vacation_type == "2" or vacation_type == "3":
        return vacation_type
    else:
        return "ERROR: Type number should be 1, 2 or 3"


def create_request(first_name, surname, from_date, to_date):
    vacation_type = select_vacation()
    if vacation_type == "1":
        return vacation_pattern(first_name, surname, from_date, to_date)
    elif vacation_type == "2":
        return sick_leave_pattern(first_name, surname, from_date, to_date)
    elif vacation_type == "3":
        return day_off_pattern(first_name, surname, from_date, to_date)
    else:
        return vacation_type


print(create_request(first_name, surname, from_date, to_date))
