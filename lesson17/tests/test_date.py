import pytest


def pytest_generate_tests(metafunc):
     with open("datafile.txt",'r') as file:
        test_data = tuple(line for line in file)

     metafunc.parametrize("a,b,expected", test_data)


def test_diff(a, b, expected):
    diff = a - b
    assert diff == expected
