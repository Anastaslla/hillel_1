from lesson17 import manwoman_classes as main
import pytest
import datetime

woman = main.Woman("Anne", "Nikolas", 163, 55, datetime.date(1963, 2, 22))
man = main.Man("Anne", "Nikolas", 163, 55, datetime.date(1972, 2, 22))
androgin = main.Androgin("Adam", "from Enneade", 190, 90, datetime.date(1772, 2, 22))


class Children:
	def __init__(self, name="Unnamed", mother=woman, father=man, height=15, weight=3, sex=main.Woman):
		self.name = name
		self.mother = mother
		self.father = father
		self.height = height
		self.weight = weight
		self.sex = sex
		self.child = mother.birth(name, height, weight, sex, father)


@pytest.mark.parametrize("sex, expected_sex", [(main.Androgin, "Androgin from Enneade"),
                                               (main.Man, "Man"), (main.Woman, "Woman")])
def test_sex(sex, expected_sex):
	new_child = Children(sex=sex)
	assert new_child.child.sex == expected_sex
	f"Incorrect sex: {new_child.child.sex}. Should be {expected_sex}"


@pytest.mark.parametrize("name, mother, father, height, weight, sex", [
	("Oliver", woman, man, 18, 3, main.Man),
	("Oversizy", androgin, androgin, 100000000, 10000000, main.Androgin),
	("Tiny", woman, androgin, 0, 0.001, main.Androgin)])
def test_child_creation(name, mother, father, height, weight, sex):
	child = mother.birth(name, height, weight, sex, father)
	assert child.name == name
	assert child.mother == mother
	assert child.father == father
	assert child.height == height
	assert child.weight == weight
	assert child.surname == father.surname
