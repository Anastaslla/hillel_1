from lesson17 import manwoman_classes as main
import pytest
import datetime

def create_child(mother, father, is_married: bool):
	mom = mother("Anne", "Mother's surname", 163, 55, datetime.date(1963, 2, 22))
	dad = father("Stas", "Father's surname", 163, 55, datetime.date(1972, 2, 22))
	if is_married == True:
		mom.marry(dad)
	child = mom.birth("name", 15, 2, mother, dad)
	return mom, dad, child

1
def pytest_generate_tests(metafunc):
	create_child(main.Woman, main.Man, False)
	metafunc.parametrize("mom, dad, child", [create_child(main.Woman, main.Man, False),
											 create_child(main.Woman, main.Man, True),
											 create_child(main.Androgin, main.Man, False),
											 create_child(main.Androgin, main.Man, True),
											 create_child(main.Androgin, main.Androgin, False),
											 create_child(main.Androgin, main.Androgin, True),
											 create_child(main.Woman, main.Androgin, False),
											 create_child(main.Woman, main.Androgin, True)
											 ])

def test_child_surname(mom, dad, child):
	assert child.surname == dad.surname
