from lesson17 import manwoman_classes as main
import pytest
import datetime
from dataclasses import dataclass


@dataclass
class Test_data:
    birth_date: datetime.date
    age: int

@pytest.fixture
def human_creating_data():
    test_data = Test_data(datetime.date(1989, 4, 20), 32)
    return test_data

@pytest.fixture
def human_changing_data():
    test_data = Test_data(datetime.date(1980, 4, 20), 41)
    return test_data


@pytest.fixture
def create_human(human_creating_data):
    adam_from_enneade = main.Androgin("Adam", "from Enneade", 10, 10, human_creating_data.birth_date)
    return adam_from_enneade

@pytest.fixture
def change_human(human_creating_data, human_changing_data):
    adam_from_enneade2 = main.Androgin("Adam", "from Enneade", 10, 10, human_creating_data.birth_date)
    adam_from_enneade2.birth_date = human_changing_data.birth_date
    return adam_from_enneade2


def test_age(create_human, human_creating_data):
    human_age = create_human.age
    assert human_age == human_creating_data.age
    f"Incorrect human age: {human_age}. Should be {human_creating_data.age}"

def test_age_changing(change_human, human_changing_data):
    human_age = change_human.age
    assert human_age == human_changing_data.age
    f"Incorrect human age: {human_age}. Should be {human_changing_data.age}"


