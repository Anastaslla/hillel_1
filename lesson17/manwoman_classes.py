from datetime import date
from abc import ABC, abstractmethod


class Human(ABC):

	def __init__(self, name, surname, height, weight, birth_date):
		self.name = name
		self.surname = surname
		self.height = height
		self.weight = weight
		self.birth_date = birth_date
		self.__children = list()
		self.sex = None
		self.mother = None
		self.father = None

	@property
	def age(self):
		return int(((date.today() - self.birth_date).days) / 365)

	@property
	def children(self):
		return self.__children

	def eat(self, dish):
		print(f"I'm eating {dish}")

	def sleep(self):
		print("I'm sleeping")

	def run(self):
		print("I'm running…")

	@abstractmethod
	def marry(self, partner, marry_partner=True):
		pass

	def add_child(self, child):
		self.__children.append(child)


class Man(Human):

	def __init__(self, name, surname, height, weight, birth_date):
		super().__init__(
			name,
			surname,
			height,
			weight,
			birth_date
		)
		self.sex = "Man"
		self.__wife = None

	@property
	def wife(self):
		return self.__wife

	def fight(self):
		print("Buhh! Bahh! Bahhh!")

	def marry(self, partner, marry_partner=True):
		self.__wife = partner
		if marry_partner:
			self.__wife.marry(self, marry_partner=False)


import datetime


class Woman(Human):

	def __init__(self, name, surname, height, weight, birth_date):
		super().__init__(
			name,
			surname,
			height,
			weight,
			birth_date
		)
		self.sex = "Woman"
		self.__husband = None

	@property
	def husband(self):
		return self.__husband

	def marry(self, partner: Man, marry_partner=True):
		self.__husband = partner
		if marry_partner:
			self.__husband.marry(self, marry_partner=False)

	def birth(self, name, height, weight, sex, father: Man):
		child = sex(
			name,
			father.surname,
			height,
			weight,
			datetime.date.today()
		)

		child.mother = self
		child.father = father

		self.add_child(child)
		father.add_child(child)
		return child


class Androgin(Man, Woman):

	def __init__(self, name, surname, height, weight, birth_date):
		Man.__init__(
			self,
			name,
			surname,
			height,
			weight,
			birth_date)

		Woman.__init__(
			self,
			name,
			surname,
			height,
			weight,
			birth_date)
		self.sex = "Androgin from Enneade"

	def marry(self, partner: Human, marry_partner=True):
		if isinstance(partner, Man):
			self.__husband = partner
			if marry_partner:
				self.__husband.marry(self, marry_partner=False)
		elif isinstance(partner, Woman):
			self.__wife = partner
			if marry_partner:
				self.__wife.marry(self, marry_partner=False)


woman = Woman("Anne", "Nikolas", 163, 55, datetime.date(1963, 2, 22))
man = Man("Stas", "Nikolas", 163, 55, datetime.date(1972, 2, 22))
androgin = Androgin("Adam", "from Enneade", 190, 90, datetime.date(1772, 2, 22))
man2 = Man("Stas", "Nikolas", 163, 55, datetime.date(1972, 2, 22))
androgin2 = Androgin("Adam", "from Enneade", 190, 90, datetime.date(1772, 2, 22))
print(man.wife)
print(woman.husband)
man.marry(woman)
print(man.wife)
print(woman.husband)
man.marry(androgin)
print(man.wife)
print(androgin.husband)
print(androgin.wife)
androgin.marry(man)
print(man.wife)
print(androgin.husband)
print(androgin.wife)
androgin.marry(androgin2)
print(androgin2.husband)
print(androgin2.wife)