from selenium import webdriver
import pytest
import time

class TestEarrings():
    def setup_method(self, method):
        self.driver = webdriver.Chrome("chromedriver.exe")
        self.driver.get("https://www.wish.com/?hide_login_modal=true")
        time.sleep(3)

    def teardown_method(self, method):
        self.driver.quit()

    def find_data_testid(self, locator):
        return self.driver.find_element_by_xpath(f"//*[@data-testid='{locator}']")

    def test_login_form(self):
        
        self.driver.set_window_size(1500, 824)
        # fill in login form
        self.find_data_testid("login-username").send_keys("text")
        self.find_data_testid("login-password").send_keys("text")
        self.find_data_testid('login-button').click()
        
    def test_singin_form(self):
        self.find_data_testid("signup-firstname").send_keys("text")
        self.find_data_testid("signup-lastname").send_keys("text")
        self.find_data_testid("signup-email").send_keys("text")
        self.find_data_testid("signup-password").send_keys("text")

    @pytest.mark.parametrize("button_name", (("login-facebook"), ("login-google"), ("login-apple")))
    def test_login_by_socialnetwork(self, button_name):
        self.find_data_testid("login-facebook").click()

    @pytest.mark.parametrize("left_button, right_button", (("login-facebook", "login-google"), ("login-google", "login-apple")))
    def test_socialbuttons_position(self, left_button, right_button):
        assert self.driver.find_element_by_xpath(f"//*[@data-testid='{left_button}']/following-sibling::*") == self.find_data_testid(right_button)

    def test_footer(self):
        assert self.driver.find_element_by_xpath("//*[@id='footer']").text == "Contact, Policies & More"
