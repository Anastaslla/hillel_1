from lesson17_1.fun import divi
import pytest


def pytest_generate_tests(metafunc):
	with open("some_values", "r") as f:
			testdata = [tuple(int(j) for j in i.split()) for i in f.readlines()]
	metafunc.parametrize("a, b, expected_result", testdata)


# @pytest.mark.parametrize("a, b, expected_result", [(6, 4, 10), (2, 2, 4)])
def test_divi_positive(a, b, expected_result):
	assert divi(a, b) == expected_result


# @pytest.mark.parametrize("a, b, exception_name", [(5, 0, ZeroDivisionError), (2, "2", TypeError)])
# def test_divi_exception(a, b, exception_name):
# 	with pytest.raises(exception_name):
# 		divi(a, b)
