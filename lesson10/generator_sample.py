def f_gen():
    n = 1
    while True:
        yield n**2
        n += 1


g = f_gen()
next(g)

for i in g:
    print(i)
    if i > 10:
        g.close()

