from parcer import words_generator
import random


def select_a_word(word_set):
    return random.choice(list(word_set))


def generator(current, word_set):
    word_set.remove(current)
    while len(word_set) > 1:
        for word in word_set:
            if current[-1] == word[0]:
                yield word
                current = word
                word_set.remove(word)
                break
        else:
            break


def run_word_game():
    generated_words = set(words_generator())
    print(len(generated_words))
    selected_word = select_a_word(generated_words)
    print("[GAME STARTS] Random word:", selected_word)
    g = generator(selected_word, generated_words)
    for i in g:
        print(i)
    print("[GAME OVER]")


run_word_game()