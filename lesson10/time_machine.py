import random
from datetime import date

user_selected_date = date(1979, 2, 3)


def year_century(year):
    min_year = year//100 * 100
    max_year = min_year+100
    return min_year, max_year


def year_decade(year):
    min_year = year//10 * 10
    max_year = min_year + 10
    return min_year, max_year


def years_generator(user_date):
    i = 0
    while i <= 29:
        a = random.randint(*year_century(user_date.year))
        i += 1
        yield a


def time_machine_printer(user_date):
    for random_year in years_generator(user_date):
        if random_year == user_date.year:
            yield f"YOU WIN!!! Your date: {random_year},{user_date.month},{user_date.day}"
            break
        elif random_year in range(*year_decade(user_date.year)):
            yield f"{random_year},{user_date.month},{user_date.day}"


g = time_machine_printer(user_selected_date)
for i in g:
    print(i)

