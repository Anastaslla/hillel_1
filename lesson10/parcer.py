import requests


# - Get the text from quotes.txt
def get_text():
    text = requests.get(
        "https://raw.githubusercontent.com/ranjith19/random-quotes-generator/master/quotes.txt").text
    return text


# create quote list without author name
def create_quote_list(text):
    return [quote for quote in text.split("\n.\n")]


# remove symbols and digits from words
def pure_words(word):
    word = "".join([symbol for symbol in word if symbol in "abcdefghijklmnopqrstuvwxyz"])
    return word


# create word set from the quote list
def create_word_set(quotes):
    return [pure_words(word) for word in ''.join(quotes).lower().replace(".", " ").replace("'", " ").split() if len(pure_words(word)) > 1]


# create word set from the quote list
def words_generator():
    text = get_text()
    for quote in create_quote_list(text):
        quote = quote.lower().replace(".", " ").replace("'", " ").replace("-", " ").split()
        for word in quote:
            if len(pure_words(word)) > 1:
                yield pure_words(word)



# sett = set(i for i in words_generator())
# print(sett)
# print(len(sett))