import random
from datetime import date


def year_century(year):
    min_year = year//100 * 100
    max_year = min_year+100
    return min_year, max_year


def year_decade(year):
    min_year = year//10 * 10
    max_year = min_year + 10
    return min_year, max_year


def time_machine(user_date):
    i = 0
    while True:
        if i >= 30:
            yield "Not enough energy. Please, try again later."
            break
        random_year = random.randint(*year_century(user_date.year))
        if random_year == user_date.year:
            yield "YOU WIN! " + str(random_year)
            break
        elif random_year in range(*year_decade(user_date.year)):
            i += 1
            yield random_year
        else:
            i += 1
            yield "..."


def users_input():
    user_input = [int(i) for i in input("Write a date in format 1979 2 29").split()]
    user_date = date(*user_input)
    return user_date


user_selected_date = users_input()
g = time_machine(user_selected_date)
for i in g:
    print(i)

