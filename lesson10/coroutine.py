from parcer import words_generator


def find_word():
    word_set = set(words_generator())
    try:
        while True:
            user_word = (yield)
            for word in word_set:
                if user_word[-1] == word[0]:
                    print(word)
                    break
    except GeneratorExit:
        print("Exiting game")


def game_round():
    coroutine = find_word()
    next(coroutine)
    while True:
        user_word = input("Write a word to play, or q for exit\n")
        if user_word == "q":
            coroutine.close()
            break
        else:
            coroutine.send(user_word)

game_round()
