def bare_bones():
    print("My first Coroutine!")
    while True:
        value = (yield)
        print(value)

coroutine = bare_bones()
next(coroutine)
coroutine.send("1")
coroutine.send("2")
coroutine.close()