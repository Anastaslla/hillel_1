import time


def counter(seconds):
    while seconds >= 0:
        yield seconds
        seconds -= 1
        time.sleep(1)


seconds_amount = 5
for i in counter(seconds_amount):
    print(i)
