import datetime
from datetime import date


class Person:
    population = 0

    def __init__(self,
                 name: str,
                 surname: str,
                 birth_date: tuple
                 ):
        self.name = name
        self.surname = surname
        self.birth_date = datetime.date(*birth_date)
        self.__class__.population += 1

    @property
    def age(self):
        today = date.today()
        years = today.year - self.birth_date.year
        if today.month > self.birth_date.month:
            age = years
        elif today.month == self.birth_date.month and today.day >= self.birth_date.day:
            age = years
        else:
            age = years - 1
        return age

    def eat(self, meal):
        return f"I'm having {meal}"

    def speep(self):
        return f"...zzz"

    def speak(self, phrase: str):
        return f"{self.name} says: - {phrase}"

    def introducing(self, meal, place):
        str = f"Hello, My name is {self.name} {self.surname}. I'm {self.age} years old. {self.eat(meal)} and {self.lay(place)}."
        return self.speak(str)

    def walk(self):
        return f"I'm walking"

    def stay(self):
        return f"I'm staying"

    def lay(self, place):
        return f"I'm laying on the {place}"


print("Now population is:", Person.population)

Anne = Person("Anne", "Nikolas", (1989, 1, 20))
print(Anne.introducing("dinner", "beach"))
print("Now population is:", Person.population)

Stas = Person("Stas", "Nikolas", (1983, 12, 25))
print(Stas.introducing("lunch", "sofa"))
print("Now population is:", Person.population)

